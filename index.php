<!-- 
    // Exercise 8
    // Converting mysqli to mysqliDb
-->
<?php
// Create connection to the database
require 'vendor/autoload.php';
$db = new mysqliDb('localhost', 'root', '', 'p8_exercise_backend');

// Create - Insert data into the employee table
if (isset($_POST['btnSubmit'])) {
    $data = [
        'first_name' => $_POST['first_name'],
        'last_name' => $_POST['last_name'],
        'middle_name' => $_POST['middle_name'],
        'birthday' => $_POST['birthday'],
        'address' => $_POST['address']
    ];

    // Insert data into the employee table
    $id = $db->insert('employee', $data);

    if ($id) {
        echo "<script>alert('Record created successfully');</script>";
    } else {
        echo "Error: " . $db->getLastError();
    }
}

//Update - update the address of employee
if (isset($_POST['btnUpdate'])) {
    $updateId = $_POST['update_id'];
    $newAddress = $_POST['new_address'];

    $data = ['address' => $newAddress];
    $db->where('id', $updateId)->update('employee', $data);

    if ($db->count > 0) {
        echo "<script>alert('Address updated successfully');</script>";
        header("Refresh:2");
    } else {
        echo "Error updating record: " . $db->getLastError();
    }
}

//Delete - deletes th selected employee
if (isset($_POST['btnDelete'])) {
    $deleteId = $_POST['delete_id'];

    $db->where('id', $deleteId)->delete('employee');

    if ($db->count > 0) {
        echo "<script>alert('Employee deleted successfully');</script>";
        header("Refresh:2");
    } else {
        echo "Error deleting record: " . $db->getLastError();
    }
}

// Read - Retrieve first name, last name, and birthday of all employees
$employees = $db->get('employee', null, ['id', 'first_name', 'last_name', 'birthday']);

// Close connection
$db->disconnect();
?>

<!-- Path: index.php -->
<html>
<head>
    <title>Exercise 7</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <form method="POST" action="#">
        <fieldset>
            <legend>Employee Information</legend>
            <label for="first_name">First Name:</label>
            <input type="text" id="first_name" name="first_name" required>
            
            <label for="last_name">Last Name:</label>
            <input type="text" id="last_name" name="last_name" required>
            
            <label for="middle_name">Middle Name:</label>
            <input type="text" id="middle_name" name="middle_name" required>
            
            <label for="birthday">Birthday:</label>
            <input type="date" id="birthday" name="birthday" required>
            
            <label for="address">Address:</label>
            <input type="text" id="address" name="address" required>

            <button type="submit" name="btnSubmit">Submit</button>
        </fieldset>
    </form>
    <br><br>
    <h2>Employees</h2>
    <table>
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Birthday</th>
                <th>Actions</th>
            </tr>
        </thead>
        <?php if (!empty($employees)): ?>
            <tbody>
                <?php foreach ($employees as $employee): ?>
                    <tr>
                        <td><?= $employee['first_name'] ?></td>
                        <td><?= $employee['last_name'] ?></td>
                        <td><?= $employee['birthday'] ?></td>
                        <td class='action-buttons'>
                            <form method='POST'>
                                <input type='hidden' name='update_id' value='<?= $employee['id'] ?>'>
                                <input type='text' name='new_address' placeholder='New Address' required>
                                <button type='submit' name='btnUpdate'>Update</button>
                            </form>
                            <form method='POST'>
                                <input type='hidden' name='delete_id' value='<?= $employee['id'] ?>'>
                                <button type='submit' name='btnDelete'>Delete</button>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php else: ?>
            <h4>No employees found. Add one to get started.</h4>
        <?php endif; ?>
</body>

</html>
